package com.project.sample_springboot.service;

import com.project.sample_springboot.model.Employee;
import com.project.sample_springboot.repository.EmployeeRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
    @Autowired
    EmployeeRespository employeeRespository;

    public List<Employee> getEmployees(){
        return employeeRespository.findAll();
    }

    public String saveEmployee(Employee employee){
        if(employeeRespository.existsById(employee.getId())){
            return "Couldn't save data. Id Already Exists.";
        }
        else{
            employeeRespository.save(employee);
            return "Employee data saved successfully.";
        }

    }

}
